import * as CopyWebpackPlugin from 'copy-webpack-plugin'
import * as ExtractTextPlugin from 'extract-text-webpack-plugin'
import * as path from 'path'
import * as TypedocWebpackPlugin from 'typedoc-webpack-plugin'
import * as webpack from 'webpack'
import * as WebpackBrowserPlugin from 'webpack-browser-plugin'

module.exports = {
  entry: {
    main: './src/app/entry.tsx',
    style: './src/app/style/index.less'
  },
  devtool: 'source-map',
  plugins: [
    new webpack.BannerPlugin('Wub-a-lub-a-dub-dub!!!'),
    new CopyWebpackPlugin([{ from: 'src/public' }]),
    new ExtractTextPlugin('assets/css/styles.css'),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({ sourceMap: true }),
    new TypedocWebpackPlugin({
      jsx: true,
      target: 'es6',
      exclude: '**/__templates__/**/*.*',
      ignoreCompilerErrors: false,
      includeDeclarations: false,
      experimentalDecorators: false,
      baseUrl: './src'
    }, './src')
  ],
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js'],
    modules: [
      'node_modules',
      path.resolve(__dirname, 'src')
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'assets/js/[name].js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader'
      },
      {
        test: /\.tsx?$/,
        exclude: path.resolve(__dirname, 'node_modules'),
        include: path.resolve(__dirname, 'src'),
        use: [
          { loader: 'awesome-typescript-loader' }
        ]
      },
      {
        test: /\.less$/,
        include: path.resolve(__dirname, 'src/app/style'),
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', options: { minimize: true, importLoaders: 2, sourceMap: true } },
            { loader: 'postcss-loader', options: { sourceMap: true } },
            { loader: 'less-loader', options: { sourceMap: true, strictMath: true } },
          ]
        })
      },
      {
        test: /\.less$/,
        exclude: path.resolve(__dirname, 'src/app/style'),
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader', options: { modules: true, importLoaders: 2, sourceMap: true } },
          { loader: 'postcss-loader', options: { sourceMap: true } },
          { loader: 'less-loader', options: { sourceMap: true, strictMath: true } },
        ],
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        use: [
          { loader: 'url-loader', options: { limit: '100000' } }
        ]
      }
    ]
  }
}
