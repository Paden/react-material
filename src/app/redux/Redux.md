# Redux Intro

Uni-direction data propogation scheme. Note, this is an overview of how redux is used in general. This application encapsulates much of this in ActionCreator.ts. Do NOT use this approach in this application. This only serves as a refresher. See ActionCreator.ts to implement a ActionCreator/Reducer.

```typescript
import * as Redux from 'redux'

/** 
 * -- Definitions --
 * Action: an event. Must be unique!
 * Reducer: a function that given the current state 
 *          and an action, returns the new state
 */
const uptimeReducer = (state = { count: 0 }, action) => {
  switch(action.type) {
    case 'UPTIME/INCREMENT':
      return { count: state.count++ }
    case 'UPTIME/DECREMENT':
      return { count: state.count-- }
    default:
      return state
  }
}

/**
 * Most applications contain many reducers.
 * Combining them creates an aggregated state.
 * In our example, I only use one reducer and give it
 * the attribute "Uptime." Meaning the state for the
 * store will be: { Uptime: {count: 0} }
 */
const allReducers = Redux.combineReducers({
  Uptime: uptimeReducer
})

/*
 * The application's state container.
 */
const store = Redux.createStore(allReducers)

/**
 * Subscribe to store/state changes. The callback
 * is triggered every state change.
 */
store.subscribe(() => {
  const state = store.getState()
  console.log('New Count', state.Uptime.count)
})

/**
 * -- Definitions --
 * ActionCreator: Dispatches actions to the store
 */
const DispatchIncrementAction = () => {
  store.dispatch({ type: 'UPTIME/INCREMENT'})
}

/**
 * --Example--
 * 1. ActionCreator -> Dispatches action to Store
 * 2. Store -> Sends action to all reducers
 * 3. Reducers -> return new state given the action
 * 4. If a new state is returned, store subscribers are 
 *    notified
 */
```


