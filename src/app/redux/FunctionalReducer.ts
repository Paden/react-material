import * as Redux from 'redux'

export interface IReducerAction extends Redux.Action {
  type: string
}

type IReducer<S, A extends IReducerAction = IReducerAction> = (state: S, action: A) => S
interface ITypeReducerMap<S, A extends IReducerAction = IReducerAction> {
  [index: string]: IReducer<S, A>
}
/**
 * A functional class for reducers
 * Example:
 *
 * const indexReducer = new FunctionalReducer<{ index: number }>({ index: 0})
 *   .whenTypeIs('index/INCREMENT', (state, action) => {
 *     return { index: state.index + 1 }
 *   })
 *   .whenTypeIs('index/DECREMENT', (state, action) => {
 *     return { index: state.index + 0 }
 *   })
 *
 * indexReducer.getReducerFunction() // use for Redux.combineReducers()
 */
export class FunctionalReducer<IState, IAction extends IReducerAction = IReducerAction> {
  private typeReducerMap: ITypeReducerMap<IState, IAction> = {}
  private defaultReducer: IReducer<IState, IAction>

  constructor(private initialState?: IState) { }

  /**
   * Returns aggregate reducer function for use with Redux.combineReducers()
   */
  getReducerFunction(): IReducer<IState, IAction> {
    return (state = this.initialState, action) => {
      if (action.type in this.typeReducerMap) {
        const typeReducer = this.typeReducerMap[action.type]

        return typeReducer(state, action)
      }

      if (typeof this.defaultReducer === 'function') {
        return this.defaultReducer(state, action)
      }

      return state
    }
  }

  /**
   * Add reducer for a given action type
   * @param type
   * @param typeReducer
   */
  whenTypeIs(type: string, typeReducer: IReducer<IState, IAction>): this {
    if (type in this.typeReducerMap) {
      throw new Error(`${type} must be unique`)
    }

    this.typeReducerMap[type] = typeReducer

    return this
  }

  setDefaultReducer(defaultReducer: IReducer<IState, IAction>): this {
    this.defaultReducer = defaultReducer

    return this
  }
}
