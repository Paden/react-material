// tslint:disable-next-line:no-reference-import
/// <reference types="jest" />

import { AppActionCreator } from 'app/redux/App/actions'
import { createUniqueStore, IAppState } from 'app/redux/App/store'
import { UserActionCreator } from 'app/redux/User/actions'
import * as Redux from 'redux'

describe('AppActionCreator', () => {

  let store: Redux.Store<IAppState>
  let initState: IAppState
  let appActions: AppActionCreator
  let uptimeActions: UserActionCreator

  beforeAll(() => {
    store = createUniqueStore()
    initState = store.getState()
    appActions = new AppActionCreator(store)
    uptimeActions = new UserActionCreator(store)
  })

  it('Resets App', () => {
    expect.assertions(2)

    return uptimeActions.login('demo', 'demo')
      .then(() => {
        const newState = store.getState()

        expect(newState.User.username).not.toEqual(initState.User.username)

        return appActions.reset()
      })
      .then(() => {
        const newState = store.getState()

        expect(newState).toEqual(initState)
      })

  })
})
