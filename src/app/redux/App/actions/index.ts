import { ActionCreator } from 'app/redux/ActionCreator'
import * as AppReducerModule from 'app/redux/App/reducer'

class AppActionCreator extends ActionCreator<AppReducerModule.IAppState> {
  reset() {
    return this.dispatch({ type: AppReducerModule.ActionTypes.RESET })
  }
}

const AppActions = new AppActionCreator()

export default AppActions
export {
  AppActions,
  AppActionCreator
}
