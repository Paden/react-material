import { ActionTypes, AppReducer, IAppState } from 'app/redux/App/reducer'
import * as Redux from 'redux'
import { logger } from 'redux-logger'
import thunk from 'redux-thunk'

const createUniqueStore = () => {
  return Redux.createStore<IAppState>(
    AppReducer.getReducerFunction(),
    Redux.applyMiddleware(logger, thunk)
  )
}
/**
 * This file grabs the reducers and creates the state for app's store.
 * The store can be subscribed/oberserved for state changes in containers.
 */
const store = createUniqueStore()

//For development, reducers will be hot-reloaded
if (module.hot) {
  module.hot.accept('./reducer/index', () => {
    const reducersModule = require('./reducer/index') as any
    store.replaceReducer(reducersModule.default.getReducerFunction())
  })
}

export {
  store,
  IAppState,
  createUniqueStore
}
