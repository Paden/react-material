// tslint:disable-next-line:no-reference-import
/// <reference types="jest" />

import * as AppReducerModule from 'app/redux/App/reducer'

describe('App Reducer', () => {

  it('App resets', () => {
    const reducer = AppReducerModule.AppReducer.getReducerFunction()
    const initState: AppReducerModule.IAppState = {
      User: { token: 'asdf', username: 'asdf' }
    }

    const resetAction: AppReducerModule.IAppAction = {
      type: AppReducerModule.ActionTypes.RESET
    }

    const incrementedState = reducer(initState, resetAction)

    expect(incrementedState).toEqual({
      User: { token: null, username: null, isInit: false }
    })
  })

})
