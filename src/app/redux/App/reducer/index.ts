import { FunctionalReducer, IReducerAction } from 'app/redux/FunctionalReducer'
import * as UserReducerModule from 'app/redux/User/reducer'
import * as Redux from 'redux'

/**
 * This files combines all reducers which creates the app's state
 *
 * To add a new reducer:
 * 1. Add reducer's interface state to IAppState
 * 2. Add reducer method to allReducers
 */
interface IAppState {
  User: UserReducerModule.IUserState
}

interface IAppAction extends IReducerAction {
  type: string
}

const allReducers = Redux.combineReducers<IAppState>({
  User: UserReducerModule.functionalReducer.getReducerFunction()
})

const ActionTypes = {
  RESET: 'app/RESET'
}

const AppReducer = new FunctionalReducer<IAppState>()
  .setDefaultReducer((state, action) => {
    return allReducers(state, action)
  })
  .whenTypeIs(ActionTypes.RESET, (state, action) => {
    state = undefined

    return allReducers(state, action)
  })

export default AppReducer //for hot-reload
export {
  AppReducer,
  IAppState,
  ActionTypes,
  IAppAction
}
