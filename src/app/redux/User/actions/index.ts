import { ActionCreator } from 'app/redux/ActionCreator'
import { ActionTypes, IUserAction, IUserState } from 'app/redux/User/reducer'

class UserActionCreator extends ActionCreator<IUserState, IUserAction> {
  private static LOCAL_STORAGE_KEY = 'U$3R'

  init() {
    try {
      const userJSONStr = localStorage.getItem(UserActionCreator.LOCAL_STORAGE_KEY)
      const user = JSON.parse(userJSONStr) as IUserState
      this.login(user.username, user.token)

    } catch (noProperLoginData) {
      this.dispatch(
        { type: ActionTypes.INIT }
      )
    }
  }

  login(username: string, token: string) {

    return this.dispatchAsync((getState) => {
      const isGoodLogin = username === 'demo' && token === 'demo'
      const user: IUserState = { username, token, isInit: true }
      const type = ActionTypes.LOGIN

      if (isGoodLogin) {
        try {
          const userStr = JSON.stringify(user)
          localStorage.setItem(UserActionCreator.LOCAL_STORAGE_KEY, userStr)
        } catch (e) {
          // tslint:disable-next-line:no-console
          console.error('Unable to save user credentials')
        }

        return Promise.resolve(
          { type, user }
        )
      } else {
        return Promise.reject('Incorrect login credentials')
      }
    })
  }

  logout() {
    return this.dispatchAsync((getState) => {
      try {
        localStorage.setItem(UserActionCreator.LOCAL_STORAGE_KEY, null)
      } catch (e) {
        // tslint:disable-next-line:no-console
        console.error('Unable to log user out')
      }

      return Promise.resolve(
        { type: ActionTypes.LOGOUT }
      )
    })
  }
}

const UserActions = new UserActionCreator()

export {
  UserActions,
  UserActionCreator
}
