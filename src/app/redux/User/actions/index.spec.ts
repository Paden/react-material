// tslint:disable-next-line:no-reference-import
/// <reference types="jest" />

import { createUniqueStore, IAppState } from 'app/redux/App/store'
import * as Redux from 'redux'
import { UserActionCreator } from './index'

describe('User Actions', () => {

  let store: Redux.Store<IAppState>
  let initState: IAppState
  let actions: UserActionCreator

  beforeAll(() => {
    store = createUniqueStore()
    initState = store.getState()
    actions = new UserActionCreator(store)
  })

  it('login works', () => {
    expect.assertions(2)

    return actions.login('demo', 'demo')
      .then(() => {
        const newState = store.getState()

        expect(newState.User.username).not.toEqual(initState.User.username)
        expect(newState.User.token).not.toEqual(initState.User.token)
      })
  })

  it('logout works', () => {
    expect.assertions(2)

    return actions.login('demo', 'demo')
      .then(() => actions.logout())
      .then(() => {
        const newState = store.getState()

        expect(newState.User.username).toEqual(initState.User.username)
        expect(newState.User.token).toEqual(initState.User.token)
      })
  })
})
