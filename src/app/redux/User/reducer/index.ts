import { FunctionalReducer, IReducerAction } from 'app/redux/FunctionalReducer'
import * as Redux from 'redux'

interface IUserState {
  username: string
  token: string
  isInit?: boolean
}

interface IUserAction extends IReducerAction {
  type: string
  user?: IUserState
}

const ActionTypes = {
  LOGIN: 'user/LOGIN',
  LOGOUT: 'user/LOGOUT',
  INIT: 'user/INIT'
}

const initialState: IUserState = {
  username: null,
  token: null,
  isInit: false,
}

const functionalReducer = new FunctionalReducer<IUserState, IUserAction>(initialState)
  .whenTypeIs(ActionTypes.INIT, () => {
    return { username: null, token: null, isInit: true }
  })
  .whenTypeIs(ActionTypes.LOGIN, (state, { user: { username, token } }) => {
    return { username, token, isInit: true }
  })
  .whenTypeIs(ActionTypes.LOGOUT, (state, action) => {
    return { username: null, token: null, isInit: true }
  })

export {
  functionalReducer,
  ActionTypes,
  IUserState,
  IUserAction
}
