// tslint:disable-next-line:no-reference-import
/// <reference types="jest" />

import * as ReducerModule from './index'

describe('User Reducer', () => {

  it('increments count', () => {
    const reducer = ReducerModule.functionalReducer.getReducerFunction()
    const initState: ReducerModule.IUserState = {
      username: null, token: null
    }

    const incrementedState = reducer(initState, {
      type: ReducerModule.ActionTypes.LOGIN,
      user: {
        token: 'demo',
        username: 'demo',
      }
    })

    expect(incrementedState.username).toEqual('demo')
    expect(incrementedState.token).toEqual('demo')

  })
})
