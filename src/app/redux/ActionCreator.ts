import { IAppState } from 'app/redux/App/reducer'
import { store } from 'app/redux/App/store'
import { IReducerAction } from 'app/redux/FunctionalReducer'
import * as Redux from 'redux'
import * as ReduxThunk from 'redux-thunk'

type ActionPromiseCallback<A> = (getState: () => IAppState) => Promise<A>
type FetchActionPromiseCallback<A, U> = (getState: () => IAppState, json: U) => Promise<A>

/**
 * ActionCreator class has
 * If that sounds Greek, See Redux.md at app/redux/Redux.md for a redux refresher.
 *
 */
export abstract class ActionCreator<IState, IAction extends IReducerAction = IReducerAction> {

  constructor(private actionStore = store) { }
  /**
   * Http request
   * @param requestInfo
   */
  protected fetchJSON<T>(requestInfo: RequestInfo): Promise<T> {
    return window.fetch(requestInfo)
      .then((response) => response.json())
      .then((json) => json as T)
  }

  /**
   * window.setTimeout in promise form
   * @param timeout milliseconds to timeout
   */
  protected setTimeoutPromise(timeout: number) {
    return new Promise<void>((resolve, reject) => {
      window.setTimeout(() => resolve(), timeout)
    })
  }

  /**
   * Synchronous dispatch an action to the reducer method
   * @param action
   */
  protected dispatch(action: IAction) {
    return this.actionStore.dispatch<IAction>(action)
  }

  /**
   * Asynchronous dispatch an action to the reducer method
   * Example:
   * this.dispatchAsync((getState) => {
   *   return new Promise((resolve, reject) => {
   *     resolve(action)
   *   })
   * })
   * @param asyncAction ReduxThunk action
   */
  protected dispatchAsync(actionPromiseCallback: ActionPromiseCallback<IAction>) {
    return this.actionStore.dispatch<Promise<IAction>, void>((dispatch, getState) => {
      return actionPromiseCallback(getState)
        .then((action) => dispatch(action))
    })
  }

  /**
   * Fetch a JSON and then asynchronous dispatch an action to the reducer method
   * Example:
   * this.fetchJSONAndDispatchAsync<jsonInterface>('/endpoint', (getState, json) => {
   *   return new Promise((resolve, reject) => {
   *     resolve(action)
   *   })
   * })
   * @param requestInfo window.fetch's first parameter
   * @param asyncAction ReduxThunk action with json as extra parameter
   */
  protected fetchJSONAndDispatchAsync<U>(requestInfo: RequestInfo, fetchActionPromiseCallback: FetchActionPromiseCallback<IAction, U>) {
    return this.dispatchAsync((getState) => {
      return this.fetchJSON<U>(requestInfo)
        .then((json) => fetchActionPromiseCallback(getState, json))
    })
  }

  /**
   * Asynchronous dispatch an action after a timeout
   * Example:
   * this.dispatchAsyncWithTimeout(2 * 1000, (getState) => {
   *   return new Promise((resolve, reject) => {
   *     return resolve(action)
   *   })
   * })
   * @param timeout milliseconds to wait
   * @param asyncAction ReduxThunk action
   */
  protected dispatchAsyncWithTimeout(timeout: number, actionPromiseCallback: ActionPromiseCallback<IAction>) {
    return this.dispatchAsync((getState) => {
      return this.setTimeoutPromise(timeout)
        .then(() => actionPromiseCallback(getState))
    })
  }
}
