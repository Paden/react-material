import { IAppState, store } from 'app/redux/App/store'
import * as React from 'react'
import * as Redux from 'redux'

/**
 * Container is a React component with access to the Application's State
 *
 * By default, a Container will automatically update it's state based
 * the abstract method: mapAppStateToContainerState()
 *
 */
abstract class Container<P, S> extends React.Component<P, S> {

  private unsubscribeFromStoreFN: Redux.Unsubscribe

  /**
   * Overrider's beware! Call super.componentWillMount()
   */
  componentWillMount() {
    this.subscribeContainerToStore()
  }

  /**
   * Overrider's beware! Call super.componentWillUnmount()
   */
  componentWillUnmount() {
    this.unsubscribeContainerFromStore()
  }

  /**
   * Invokes callback when the App's state has been updated
   * @param callback
   */
  subscribe(callback: (state: IAppState) => void) {
    const invokeCallback = () => {
      const currentState = this.getState()
      callback(currentState)
    }

    //Invoke callback with initial state
    invokeCallback()

    //Subscribe to future updates
    return store.subscribe(() => invokeCallback())
  }

  /**
   * Get state from App/Store
   */
  getState() {
    return store.getState()
  }

  /**
   * An abstract method that will map the App's state to the Container's state.
   * This method will be used to auto-sync the app and the container.
   *
   * @param appState
   */
  abstract mapAppStateToContainerState(appState: IAppState): S

  /**
   * Begin sync of App State and Container State
   */
  private subscribeContainerToStore() {
    this.unsubscribeFromStoreFN = this.subscribe((appState) => {
      const newContainerState = this.mapAppStateToContainerState(appState)

      this.setState(newContainerState)
    })
  }

  /**
   * End sync of App State and Container State
   */
  private unsubscribeContainerFromStore() {
    if (typeof this.unsubscribeFromStoreFN === 'function') {
      this.unsubscribeFromStoreFN()
    }
  }
}

export {
  Container,
  IAppState
}
