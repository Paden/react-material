import { Container, IAppState } from 'app/containers/Container'
import { UserActions } from 'app/redux/User/actions'
import * as material from 'material-ui'
import TextField from 'material-ui/TextField'
import * as React from 'react'
import { Col, Grid, Row } from 'react-flexbox-grid'

interface IUserLoginState {
  isLoggedIn: boolean
  username: string
  token: string
  error: string
}

class UserLogin extends Container<{}, IUserLoginState> {
  state = {
    isLoggedIn: false,
    username: '',
    token: '',
    error: null
  }

  mapAppStateToContainerState(appState: IAppState) {
    const isLoggedIn = !!appState.User.username
    const { username, token, error } = this.state

    return { isLoggedIn, username, token, error }
  }

  login(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault()

    UserActions.login(this.state.username, this.state.token)
      .then(() => location.hash = '#/hello')
      .catch((error) => this.setState({ error }))
  }

  setPartialState(attr: string, value: any) {
    const updateState = {}
    updateState[attr] = value

    this.state.error = null

    this.setState(updateState)
  }

  render() {
    return (
      <Grid fluid>
        <Row>
          <Col mdOffset={3} md={6} lgOffset={0} lg={6}>
            <h3>Login</h3>
            <material.Paper className="animated fadeIn">
              <material.Toolbar>
                <material.ToolbarGroup firstChild={true}>
                  <material.ToolbarTitle text="Login" />
                </material.ToolbarGroup>
              </material.Toolbar>

              <form name="loginForm" onSubmit={(e) => this.login(e)}>
                <TextField floatingLabelText="Email (demo)"
                  required={true}
                  fullWidth={true}
                  onChange={(e, newValue) => this.setPartialState('username', newValue)}
                  value={this.state.username} />

                <TextField floatingLabelText="Password (demo)"
                  required={true}
                  fullWidth={true}
                  onChange={(e, newValue) => this.setPartialState('token', newValue)}
                  value={this.state.token} />

                <material.RaisedButton type="submit">
                  Login
                </material.RaisedButton>

                {this.state.error ?
                  <div style={{ marginTop: '20px' }}>
                    <strong>
                      Uh oh!
                      </strong>
                    <span>
                      &nbsp;Email and/or password are incorrect. Please try again or contact the support team.
                    </span>
                  </div>
                  : null
                }
              </form>
            </material.Paper>
          </Col>
        </Row>
      </Grid>
    )
  }
}

export {
  UserLogin
}
