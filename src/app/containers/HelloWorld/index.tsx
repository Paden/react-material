import { Container, IAppState } from 'app/containers/Container'
import { UserActions } from 'app/redux/User/actions'
import * as mui from 'material-ui'
import Paper from 'material-ui/Paper'
// tslint:disable-next-line:ordered-imports
import * as material from 'material-ui'
import * as React from 'react'
import { Col, Grid, Row } from 'react-flexbox-grid'

class HelloWorld extends React.Component<{}, {}> {

  render() {
    return (
      <material.Paper zDepth={3}>
        Hello World!

        <material.RaisedButton onClick={() => UserActions.logout()}>
          Logout
        </material.RaisedButton>
      </material.Paper>
    )
  }
}

export {
  HelloWorld
}
