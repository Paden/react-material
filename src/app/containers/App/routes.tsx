import { HelloWorld } from 'app/containers/HelloWorld';
import { UserLogin } from 'app/containers/UserLogin'
import { UserActions } from 'app/redux/User/actions'
import * as React from 'react'

const LoginRoute = {
  single: {
    name: 'Login',
    component: UserLogin,
    path: '/',
    iconClass: 'fa fa-fw fa-sign-in',
  }
}

const HelloWorldRoute = {
  single: {
    name: 'Hello World',
    component: HelloWorld,
    path: '/hello',
    iconClass: 'fa fa-fw fa-sign-out',
  }
}

const LoggedOutAppNavRoutes = [
  LoginRoute,
]

const LoggedInAppNavRoutes = [
  //DashboardRoute,
  HelloWorldRoute,
]

//Multi Route Demo
// {
//   multi: {
//     name: 'Multi Page',
//     iconClass: 'fa fa-fw fa-files-o',
//     routes: [
//       {
//         name: 'Fake Page 1',
//         path: '/fake1',
//         component: Blank,
//       },
//       {
//         name: 'Fake Page 2',
//         path: '/fake2',
//         component: Blank,
//       }
//     ]
//   }
// }

export {
  LoggedInAppNavRoutes,
  LoggedOutAppNavRoutes,
}
