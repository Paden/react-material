import createBrowserHistory from 'history/createBrowserHistory'

const AppHistory = createBrowserHistory()

export {
  AppHistory
}
