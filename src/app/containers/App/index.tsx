import { LoggedInAppNavRoutes, LoggedOutAppNavRoutes } from 'app/containers/App/routes'
import { Container, IAppState } from 'app/containers/Container'
import { store } from 'app/redux/App/store'
import { UserActions } from 'app/redux/User/actions'
import * as React from 'react'
import { Provider } from 'react-redux'
import { HashRouter, Redirect, Route, Switch } from 'react-router-dom'
import { AppHistory } from './history'

interface IAppComponentState {
  isLoggedIn: boolean
  currentNavRoute: any[]
  defaultPath: string
  isInit: boolean
}

class App extends Container<{}, IAppComponentState> {

  mapAppStateToContainerState(appState: IAppState) {
    const isInit = !!appState.User.isInit
    const isLoggedIn = !!appState.User.username
    const currentNavRoute = isLoggedIn ? LoggedInAppNavRoutes : LoggedOutAppNavRoutes
    const currentHash = location.hash.replace('#', '')
    const firstRoute = currentNavRoute[0]
    const defaultPath = firstRoute.single.path

    return { isLoggedIn, currentNavRoute, defaultPath, isInit }
  }

  componentDidMount() {
    UserActions.init()
  }

  /**
   * IBaseNavRoute to <Route /> Element
   * @param route
   * @param index
   */
  navRouteToReactRoute(route: any) {
    return (
      <Route key={route.path} exact path={route.path} component={route.component} />
    )
  }

  /**
   * AppNavRoutes to <Route /> Elements
   * @param navRoute
   * @param index
   */
  mapAppNavRoutesToReactRoutes(navRoute: any) {
    if (navRoute.single) {
      return this.navRouteToReactRoute(navRoute.single)
    }

    if (navRoute.multi) {
      return navRoute.multi.routes.map((route) => this.navRouteToReactRoute(route))
    }
  }

  render() {
    //Prevent route from being changed
    if (!this.state.isInit) {
      return null
    }

    return (
      <Provider store={store} key="provider">
        <HashRouter>
          <section className="page-container">
            <main className="page">
              <Switch>
                {this.state.currentNavRoute.map((route) =>
                  this.mapAppNavRoutesToReactRoutes(route)
                )}
                <Redirect to={this.state.defaultPath} />
              </Switch>
            </main>
          </section>
        </HashRouter>
      </Provider>
    )
  }
}

export {
  App
}
