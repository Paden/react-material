import 'core-js'; //es5, es6m es7+ polyfills
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import 'whatwg-fetch'; //window.fetch Polyfill
import { App } from './containers/App'

/**
 * Renders the Application to the DOM
 * @param AppElement
 */
const renderApp = (AppElement: React.ComponentType<any> = App) => {
  const rootEl = document.getElementById('root')
  const renderElement = (
    <AppContainer>
      <MuiThemeProvider>
        <AppElement />
      </MuiThemeProvider>
    </AppContainer>
  )

  ReactDOM.render(renderElement, rootEl)
}

//Init App
renderApp()

// Hot-reloads components while developing
if (module.hot) {
  module.hot.accept('./containers/App/index', () => {
    const updatedAppModule = require('./containers/App/index') as any

    renderApp(updatedAppModule.App)
  })
}
