# Architecture

## app/components
Simple, lower-level react components that are dependent only on their props.

## app/containers
Higher-level react components that subscribe to the applications store. Then delegating that state to the lower-level components. Some containers are used for routing at app/containers/App/routes.ts.

## app/redux
Redux actions, action creators, reducers, and store. See app/actopms/Redux.md for a refresher.

## app/style
Bootstrap theming, color palette, and site-level stylesheet.

## public
The public folder is our htdocs/www folder. Everything in there will be statically served.