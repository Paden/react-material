import * as CopyWebpackPlugin from 'copy-webpack-plugin'
import * as ExtractTextPlugin from 'extract-text-webpack-plugin'
import * as path from 'path'
import * as webpack from 'webpack'
import * as WebpackBrowserPlugin from 'webpack-browser-plugin'

const distPath = path.resolve(__dirname, 'dist')

module.exports = {
  entry: {
    main: [
      'react-hot-loader/patch', // activate HMR for React
      './src/app/entry.tsx' // the entry point of our app
    ],
  },
  devtool: 'source-map',
  plugins: [
    new webpack.BannerPlugin('Wub-a-lub-a-dub-dub!!!'),
    new webpack.HotModuleReplacementPlugin(), // enable HMR globally
    new webpack.NamedModulesPlugin(), // prints more readable module names in the browser console on HMR updates
    new CopyWebpackPlugin([{ from: 'src/public' }]),
    new WebpackBrowserPlugin({ url: 'http://localhost' }),
  ],
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js'],
    modules: [
      'node_modules',
      path.resolve(__dirname, 'src')
    ]
  },
  output: {
    path: distPath,
    filename: 'assets/js/[name].js',
    publicPath: '/'
  },
  devServer: {
    hot: true,
    publicPath: '/',
    compress: true,
    overlay: true,
    port: 8000,
    contentBase: distPath,
    watchContentBase: true,
    stats: 'minimal'
  },
  module: {
    rules: [
      {
        // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader'
      },
      {
        test: /\.tsx?$/,
        exclude: path.resolve(__dirname, 'node_modules'),
        include: path.resolve(__dirname, 'src'),
        use: [
          { loader: 'react-hot-loader/webpack' },
          { loader: 'awesome-typescript-loader' }
        ]
      },
      {
        test: /\.(css|less)$/,
        include: path.resolve(__dirname, 'src/app/style'),
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader', options: { importLoaders: 2, sourceMap: true } },
          { loader: 'postcss-loader', options: { sourceMap: true } },
          { loader: 'less-loader', options: { sourceMap: true, strictMath: true } },
        ],
      },
      {
        test: /\.(css|less)$/,
        exclude: path.resolve(__dirname, 'src/app/style'),
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader', options: { modules: true, importLoaders: 2, sourceMap: true } },
          { loader: 'postcss-loader', options: { sourceMap: true } },
          { loader: 'less-loader', options: { sourceMap: true, strictMath: true } },
        ],
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        use: [
          { loader: 'url-loader', options: { limit: '100000' } }
        ]
      }
    ]
  }
}
