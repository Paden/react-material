# React Material

## Development
- Install [Nodejs](https://nodejs.org/en/)
- Install repo dependencies:
```bash
> npm install -g yarn
> yarn
```

- To begin development:
```bash
> npm run dev
```

- To test code and format before a git commit:
```bash
> npm run beforeCommit
```

- To test production build:
```bash
> npm run buildAndServe
```

- See src/Architecutre.md for more info on development

## Recommended Development Environment

### IDE
[Visual Studio Code](https://code.visualstudio.com/)

### Visual Studio Code Plugins
[TSLint](https://marketplace.visualstudio.com/items?itemName=eg2.tslint)

## Production Build
- Install [Nodejs/NPM](https://nodejs.org/en/)
- Install repo dependencies:
```bash
> npm install -g yarn
> yarn #be sure to do this in repo's root folder
```
- To build:
```bash
> npm run build
> cd dist #production code is now inside here
```

### Test

- the following will run unit, component, lint, and format tests
```bash
> npm test
```

- To test built code:
```bash
> npm run buildAndServe
```

### Format Code
```bash
> npm run format
```
